import React, { useState } from "react";
import { ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import { CardElement, injectStripe } from "react-stripe-elements";
import LoaderButton from "./LoaderButton";
import { useFormValidate } from "../utils/useFormValidate";

function BillingForm({ isLoading, onSubmit, stripe }) {
  const [{ storage, name }, handleFieldChange] = useFormValidate({
    name: "",
    storage: ""
  });
  const [isCardCompleted, setIsCardCompleted] = useState(false);
  const [isProcessing, setIsProcessing] = useState(false);
  isLoading = isProcessing || isLoading;

  function validateForm() {
    return name !== "" && storage !== "" && isCardCompleted;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    setIsProcessing(true);

    const { token, error } = await stripe.createToken({
      name
    });

    setIsProcessing(false);
    onSubmit(storage, { token, error });
  }

  return (
    <form className="BillingForm" onSubmit={handleSubmit}>
      <FormGroup bsSize="large" controlId="storage">
        <ControlLabel>Storage</ControlLabel>
        <FormControl
          min="0"
          type="number"
          value={storage}
          onChange={handleFieldChange}
          placeholder="number of notes to store"
        />
      </FormGroup>
      <hr />
      <FormGroup bsSize="large" controlId="name">
        <ControlLabel>Cardholder&apos;s name</ControlLabel>
        <FormControl
          min="0"
          type="text"
          value={name}
          onChange={handleFieldChange}
          placeholder="name on the card"
        />
      </FormGroup>
      <ControlLabel>Credit card info</ControlLabel>
      <CardElement
        className="card-field"
        onChange={event => setIsCardCompleted(event.complete)}
        style={{
          base: { fontSize: "18px", fontFamily: '"Open Sans", sans-serif' }
        }}
      />
      <LoaderButton
        block
        type="submit"
        bsSize="large"
        isLoading={isLoading}
        disabled={!validateForm()}
      >
        Purchase
      </LoaderButton>
    </form>
  );
}
export default injectStripe(BillingForm);
