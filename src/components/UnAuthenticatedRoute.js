import React from "react";
import {Route, Redirect} from "react-router-dom";
import {queryString} from "../utils/utils";

export default function ({component: GivenComponent, appProps, ...rest}) {
    const redirect = queryString("redirect");
    return (
        <Route
            {...rest}
            render={
                props => !appProps.isAuthenticated
                    ? <GivenComponent {...props} {...appProps} />
                    : <Redirect to={redirect === "" || redirect === null ? "/" : redirect} />
            }
        />
    )

}