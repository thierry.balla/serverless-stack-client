import React from "react";
import {Route, Redirect} from "react-router-dom";

export default function ({component: GivenComponent, appProps, ...rest}) {
    return (
        <Route
            {...rest}
            render={
                props => appProps.isAuthenticated
                    ? <GivenComponent {...props} {...appProps} />
                    : <Redirect
                        to={`/login?redirect=${props.location.pathname}${props.location.search}`}
                    />
            }
        />
    )

}