import React from "react";
import {Route} from "react-router-dom";

export default function AppliedRoute({component: GivenComponent, appProps, ...rest}) {
    return (
        <Route {...rest} render={props => <GivenComponent {...props} {...appProps}/>}/>
    )
}