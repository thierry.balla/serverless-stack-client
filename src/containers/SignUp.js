import React from "react";
import { useFormValidate } from "../utils/useFormValidate";
import { useState } from "react";
import {
  ControlLabel,
  FormControl,
  FormGroup,
  HelpBlock
} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import "./signup.css";
import {Auth} from "aws-amplify";

export default function SignUp({userHasAuthenticated, history}) {
  const [
    { email, password, confirmationPassword, confirmationCode },
    handleFieldChange
  ] = useFormValidate({
    email: "",
    password: "",
    confirmationPassword: "",
    confirmationCode: ""
  });

  const [newUser, setNewUser] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  function validateForm() {
    return (
      email.length > 0 &&
      password.length > 0 &&
      password === confirmationPassword
    );
  }

  function validateConfirmationForm() {
    return confirmationCode.length > 0;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    
    setIsLoading(true);
    try {
      const newUser = await Auth.signUp({
        username: email,
        password: password
      });
      setIsLoading(false);
      setNewUser(newUser);
    } catch (e) {
      console.log(e.message);
      setIsLoading(false);
    }
  }

  async function handleConfirmationSubmit(event) {
    event.preventDefault();

    setIsLoading(true);
    try {
      await Auth.confirmSignUp(email, confirmationCode);
      await Auth.signIn(email, password);
      userHasAuthenticated(true);
      history.push('/');
    } catch (e) {
      console.log(e.message);
      setIsLoading(false)
    }
  }

  function renderConfirmationForm() {
    return (
      <form onSubmit={handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>ConfirmationCode</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            onChange={handleFieldChange}
            value={confirmationCode}
          />
          <HelpBlock>
            Please check your email for the confirmation code
          </HelpBlock>
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          isLoading={isLoading}
          disabled={!validateConfirmationForm()}
        >
          Verify
        </LoaderButton>
      </form>
    );
  }

  function renderForm() {
    return (
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            onChange={handleFieldChange}
            value={email}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>password</ControlLabel>
          <FormControl
            autoFocus
            type="password"
            onChange={handleFieldChange}
            value={password}
          />
        </FormGroup>
        <FormGroup controlId="confirmationPassword" bsSize="large">
          <ControlLabel>password</ControlLabel>
          <FormControl
            autoFocus
            type="password"
            onChange={handleFieldChange}
            value={confirmationPassword}
          />
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          isLoading={isLoading}
          disabled={!validateForm()}
        >
          Sign up
        </LoaderButton>
      </form>
    );
  }

  return (
    <div className="Signup">
      {newUser === null ? renderForm() : renderConfirmationForm()}
    </div>
  );
}
