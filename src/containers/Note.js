import React, { useEffect, useRef, useState } from "react";
import { Storage } from "aws-amplify";
import { ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { MAX_ATTACHMENT_SIZE } from "../config";
import "./Note.css";
import { s3Upload } from "../services/s3Service";
import { deleteNote, loadNote, saveNote } from "../services/apiNote";
import { formatFileName } from "../utils/utils";

export default function Note({
  match: {
    params: { id }
  },
  history
}) {
  const file = useRef(null);
  const [note, setNote] = useState(null);
  const [content, setContent] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  function validateForm() {
    return content.length > 0;
  }

  useEffect(() => {
    async function onLoad() {
      try {
        const note = await loadNote(id);
        const { content, attachment } = note;

        if (attachment) {
          note.attachmentURL = await Storage.vault.get(attachment);
        }

        setContent(content);
        setNote(note);
      } catch (e) {
        console.log(e.message);
      }
    }
    onLoad();
  }, [id]);

  async function handleSubmit(event) {
    event.preventDefault();
    let attachment;

    if (file.current && file.current.size > MAX_ATTACHMENT_SIZE) {
      console.log(
        `The file you are trying to upload is to big. Please pick a smaller file: ${MAX_ATTACHMENT_SIZE /
          1000000} MB`
      );
      return;
    }

    setIsLoading(true);
    try {
      if (file.current) {
        attachment = await s3Upload(file.current);
      }
      await saveNote({
        content,
        attachment: attachment || note.attachment,
        id
      });

      history.push("/");
    } catch (e) {
      console.log(e.message);
      setIsLoading(false);
    }
  }

  function handleFileChange(event) {
    file.current = event.target.files[0];
  }

  async function handleDelete(event) {
    event.preventDefault();
    const confirmed = window.confirm(
      "Are you sure you want to delete this note?"
    );
    if (!confirmed) {
      return;
    }

    setIsDeleting(true);
    try {
      await deleteNote(id);
      history.push("/");
    } catch (e) {
      console.log(e.message);
      setIsDeleting(false);
    }
  }

  return (
    <div className="Note">
      {note && (
        <form onSubmit={handleSubmit}>
          <FormGroup controlId="content">
            <FormControl
              value={content}
              componentClass="textarea"
              onChange={event => setContent(event.target.value)}
            />
          </FormGroup>
          {note.attachment && (
            <FormGroup>
              <ControlLabel>Attachment</ControlLabel>
              <FormControl.Static>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={note.attachmentURL}
                >
                  {formatFileName(note.attachment)}
                </a>
              </FormControl.Static>
            </FormGroup>
          )}
          <FormGroup controlId="file">
            {!note.attachment && <ControlLabel>Attachment</ControlLabel>}
            <FormControl onChange={handleFileChange} type="file" />
          </FormGroup>
          <LoaderButton
            block
            type="submit"
            bsSize="large"
            bsStyle="primary"
            isLoading={isLoading}
            disabled={!validateForm()}
          >
            Save
          </LoaderButton>
          <LoaderButton
            block
            bsSize="large"
            bsStyle="danger"
            onClick={handleDelete}
            isLoading={isDeleting}
            disabled={!validateForm()}
          >
            Delete
          </LoaderButton>
        </form>
      )}
    </div>
  );
}
