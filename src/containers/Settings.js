import React, {useState} from "react";
import {Elements, StripeProvider} from "react-stripe-elements";
import {API} from "aws-amplify";
import BillingForm from "../components/BillingForm";
import {STRIPE_KEY} from "../config";
import './Settings.css'

export default function Settings(props) {
    const [isLoading, setIsLoading] = useState(false);

    function billUser(details) {
        return API.post('notes', '/billing', {
            body: details
        })
    }
    async function handleSubmit(storage, {token, error}) {
        if (error) {
            console.log(error);
            return;
        }

        setIsLoading(true);
        try {
            await billUser({
                storage,
                source: token.id
            });
            console.log('your card has been charged successfully');
            props.history.push('/')
        } catch (e) {
           console.log(e.message);
            setIsLoading(false)
        }

    }

    return (
        <div className="Settings">
            <StripeProvider apiKey={STRIPE_KEY}>
                <Elements>
                    <BillingForm isLoading={isLoading} onSubmit={handleSubmit} />
                </Elements>
            </StripeProvider>
        </div>
    )
}