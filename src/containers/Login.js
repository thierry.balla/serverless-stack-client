import React, { useState } from "react";
import "./Login.css";
import { ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import { Auth } from "aws-amplify";
import LoaderButton from "../components/LoaderButton";
import { useFormValidate } from "../utils/useFormValidate";
export default function Login({ userHasAuthenticated, history }) {
  const [isLoading, setIsLoading] = useState(false);
  const [{ email, password }, handleFieldChange] = useFormValidate({
    email: "",
    password: ""
  });

  const validateForm = () => {
    return email.length > 0 && password.length > 0;
  };

  const handleSubmit = async event => {
    event.preventDefault();
    setIsLoading(true);
    try {
      await Auth.signIn(email, password);
      userHasAuthenticated(true);
    } catch (error) {
      setIsLoading(false);
      console.log(error.message);
    }
  };

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={handleFieldChange}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            autoFocus
            type="password"
            value={password}
            onChange={handleFieldChange}
          />
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          isLoading={isLoading}
          disabled={!validateForm()}
        >
          Login
        </LoaderButton>
      </form>
    </div>
  );
}
