import React from "react";
import { Switch } from "react-router-dom";
import AppliedRoute from "../components/AppliedRoute";
import Home from "../containers/Home";
import NotFound from "../components/NotFound";
import Login from "../containers/Login";
import SignUp from "../containers/SignUp";
import NoteCreation from "../containers/NoteCreation";
import Note from "../containers/Note";
import Settings from "../containers/Settings";
import UnAuthenticatedRoute from "../components/UnAuthenticatedRoute";
import AuthenticatedRoute from "../components/AuthenticatedRoute";

export default function Routes({ appProps }) {
  return (
    <Switch>
      <AppliedRoute path="/" exact component={Home} appProps={appProps} />
      <UnAuthenticatedRoute path="/login" exact component={Login} appProps={appProps} />
      <UnAuthenticatedRoute
        path="/signup"
        exact
        component={SignUp}
        appProps={appProps}
      />
      <AuthenticatedRoute
        path="/settings"
        exact
        component={Settings}
        appProps={appProps}
      />
      <AuthenticatedRoute
        path="/notes/new"
        exact
        component={NoteCreation}
        appProps={appProps}
      />
      <AuthenticatedRoute
        path="/notes/:id"
        exact
        component={Note}
        appProps={appProps}
      />
      <AppliedRoute component={NotFound} />
    </Switch>
  );
}
