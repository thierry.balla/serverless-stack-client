import {API} from "aws-amplify";
import config from "../config/config";

export function loadNotes() {
    return API.get(config.API_NAME, '/notes', {});
}
export function loadNote(id) {
    return API.get(config.API_NAME, `/notes/${id}`, {});
}
export function createNote(note) {
    return API.post(config.API_NAME, "/notes", {
        body: note
    });
}
export function saveNote(note) {
    return API.put(config.API_NAME, `/notes/${note.id}`, {
        body: note
    });
}
export function deleteNote(id) {
    return API.del(config.API_NAME, `/notes/${id}`, {});
}
