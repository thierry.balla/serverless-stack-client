const dev = {
    STRIPE_KEY: "pk_test_V3rT1U8iqQyRF0rOHXfO8D1600zj9pIw8B",
    s3: {
        REGION: "eu-west-1",
        BUCKET: "notes-app-2-api-dev-attachmentbucket-1mnuxqpafgcon"
    },
    apiGateway: {
        REGION: "eu-west-1",
        URL: "https://sxy0d5074c.execute-api.eu-west-1.amazonaws.com/dev"
    },
    cognito: {
        REGION: "eu-west-1",
        USER_POOL_ID: "eu-west-1_K5TEwelUs",
        APP_CLIENT_ID: "3cvv9v6v81b47as568nm8i846f",
        IDENTITY_POOL_ID: "eu-west-1:35338868-342e-45dc-8b7b-d2b5879f392b"
    }
};
const prod = {
    STRIPE_KEY: 'pk_test_V3rT1U8iqQyRF0rOHXfO8D1600zj9pIw8B',
    s3: {
        REGION: "eu-west-1",
        BUCKET: "notes-app-2-api-dev-attachmentbucket-1mnuxqpafgcon"
    },
    apiGateway: {
        REGION: 'eu-west-1',
        URL: 'https://sxy0d5074c.execute-api.eu-west-1.amazonaws.com/dev'
    },
    cognito: {
        REGION: "eu-west-1",
        USER_POOL_ID: "eu-west-1_K5TEwelUs",
        APP_CLIENT_ID: "3cvv9v6v81b47as568nm8i846f",
        IDENTITY_POOL_ID: "eu-west-1:35338868-342e-45dc-8b7b-d2b5879f392b"
    }
};

const config = process.env.REACT_APP_STAGE === 'prod' ? prod : dev;

export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    API_NAME: 'notesApi',
    ...config
};
