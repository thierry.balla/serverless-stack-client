export const API_GATEWAY_REGION = 'eu-west-3';
export const API_GATEWAY_URL = 'https://5va7dxq85f.execute-api.eu-west-3.amazonaws.com/dev';

export const  API = {
    endpoints: [
        {
            name: 'notes',
            endpoint: API_GATEWAY_URL,
            region: API_GATEWAY_REGION
        }
    ]
};
