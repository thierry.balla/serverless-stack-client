import { IDENTITY_POOL_ID } from "./auth";

export const S3_UPLOADS_BUCKET_REGION = "eu-west-3";
export const S3_UPLOADS_BUCKET_NAME = "emergence2035-notes-app-uploads";

export const Storage = {
  region: S3_UPLOADS_BUCKET_REGION,
  bucket: S3_UPLOADS_BUCKET_NAME,
  identityPoolId: IDENTITY_POOL_ID
};
