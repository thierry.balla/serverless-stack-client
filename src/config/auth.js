const COGNITO_REGION = "eu-west-1";
const COGNITO_USER_POOL_ID = "eu-west-1_XSrvgKs61";
const COGNITO_APP_CLIENT_ID = "4t4e833mq9do5m9q56lm66v9tp";
export const IDENTITY_POOL_ID = "eu-west-1:217f5838-3f94-4550-a7f9-dca548935499";
const MANDATORY_SIGN_IN = true;
export const Auth = {
  mandatorySignIn: MANDATORY_SIGN_IN,
  region: COGNITO_REGION,
  userPoolId: COGNITO_USER_POOL_ID,
  identityPoolId: IDENTITY_POOL_ID,
  userPoolWebClientId: COGNITO_APP_CLIENT_ID
};
